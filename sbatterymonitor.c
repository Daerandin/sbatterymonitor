/*********************************************************************************************************
 *                                                                                                       *
 * sbatterymonitor - a simple program to monitor laptop battery and shut down the laptop on low levels.  *
 *                                                                                                       *
 * Copyright (C) 2016-2018 Daniel Jenssen <daerandin@gmail.com>                                          *
 *                                                                                                       *
 * This program is free software: you can redistribute it and/or modify                                  *
 * it under the terms of the GNU General Public License as published by                                  *
 * the Free Software Foundation, either version 3 of the License, or                                     *
 * (at your option) any later version.                                                                   *
 *                                                                                                       *
 * This program is distributed in the hope that it will be useful,                                       *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                                        *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                                         *
 * GNU General Public License for more details.                                                          *
 *                                                                                                       *
 * You should have received a copy of the GNU General Public License                                     *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.                                 *
 *                                                                                                       *
 *                                                                                                       *
 *                                                                                                       *
 *                                                                                                       *
 * This simple program will read battery levels. If battery levels are lower than 12%, then the program  *
 * will issue the shutdown_command.                                                                      *
 *                                                                                                       *
 * It is important to check that the first three variables are correct for your system before compiling  *
 * this program.                                                                                         *
 *                                                                                                       *
 * The program accepts the following arguments:                                                          *
 *                                                                                                       *
 *     -c  continuos mode: The program will keep running, never exiting on its own                       *
 *                                                                                                       *
 *     -x  Terminate X server: This option will send SIGTERM to X server when battery levels drop below  *
 *         16%                                                                                           *
 *                                                                                                       *
 * Example: sbatterymonitor -c                                                                           *
 *                                                                                                       *
 *********************************************************************************************************/

#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <syslog.h>
#include <stdbool.h>
#include <unistd.h>
#include <signal.h>

#define MAX_NUM_CHAR 4 /* Maximum battery status is 100, which in a string would be 4 chars. */
#define PIDS_SIZE 20

static bool battery_is_charging(const char *, const char *);
static int get_battery_level(const char *);
static void report_unknown_argument(const char *);
static bool is_program_running(const char *, pid_t *, int);
static void write_to_log(const char *, bool);

int main(int argc, char **argv)
{

    /* Modify these to match the battery path on your system. */
    const char battery_path_capacity[] = "/sys/class/power_supply/BAT0/capacity";
    const char battery_path_status[] = "/sys/class/power_supply/BAT0/status";

    /* Modify this to be a valid shutdown command for your system. */
    const char shutdown_command[] = "systemctl poweroff";
    
    /* This is the string indicating that the battery is charging. It should appear similarly on most systems, but
     * it is a good idea to check the status on your system for reference. */
    const char charging_string[] = "Charging";

    int i, battery_level = 0;
    bool single_mode = true;
    bool kill_x = false;
    pid_t pids[PIDS_SIZE];

    /* Parsing arguments. Program will exit with failure if an unknown argument is encountered. */
    while (argc-- > 1) {
        
        if (**(++argv) != '-')
            report_unknown_argument(*argv);
        else {
            while (*(++(*argv))) {
                switch (**argv) {
                    case 'x':
                        kill_x = true;
                        break;
                    case 'c':
                        single_mode = false;
                        break;
                    default:
                        report_unknown_argument(*argv);
                }
            }
        }
    }

    /* Initialize pids array with zeros. */
    for (i = 0; i < PIDS_SIZE; i++)
        pids[i] = 0;

    /* This loop will run once, and repeat infinitely if the program is run in continuous mode. */
    do {

        if (!battery_is_charging(battery_path_status, charging_string)) {
    
            battery_level = get_battery_level(battery_path_capacity);

            if (battery_level < 16) {

                if (battery_level < 12) {
                    write_to_log("Shutting down system due to low battery", false);
                    system(shutdown_command);
                } else if (kill_x) {
                    if (is_program_running("Xorg", pids, PIDS_SIZE)) {
                        write_to_log("Exiting graphical environment due to low battery", false);
                        i = 0;
                        while (pids[i] && i < PIDS_SIZE)
                            kill(pids[i++], SIGTERM);
                    }
                }
            }
        }

        if (!single_mode)
            sleep(60);

    } while (!single_mode);

    exit(EXIT_SUCCESS);
}

/*****************************************************************************
 *                                                                           *
 * Reads the battery_path_status file and compares it to charging_string.    *
 * Returns true if the two match, returns false otherwise.                   *
 *                                                                           *
 * If the file can't be read, the program will exit with EXIT_FAILURE.       *
 *                                                                           *
 *****************************************************************************/
static bool battery_is_charging(const char *battery_path_status, const char *charging_string)
{

    FILE *fp;
    int c = 0;
    int i, j = (int)strlen(charging_string);

    if ((fp = fopen(battery_path_status, "r")) == NULL) {
        write_to_log("Error in battery_is_charging: Battery status file could not be opened", true);
        exit(EXIT_FAILURE);
    }

    for (i = 0; i <= j; i++) {

        if ((c = getc(fp)) == (int)'\n')
            break;

        if (c != (int)charging_string[i])
            break;
    }

    fclose(fp);

    return (c == (int)'\n' && charging_string[i] == '\0');

}

/***********************************************************************
 *                                                                     *
 * Read battery level from battery_path_capacity.                      *
 * Converts it into an integer and returns the number.                 *
 *                                                                     *
 * If the file can't be read, the program will exit with EXIT_FAILURE. *
 *                                                                     *
 * Due to some bugs I am not 100% sure about the cause of, every time  *
 * the battery capacity is not understood according to this program it *
 * will log to syslog, but continue running. Upon 3 successive         *
 * failures to understand the capacity will it terminate.              *
 *                                                                     *
 ***********************************************************************/
static int get_battery_level(const char *battery_path_capacity)
{

    FILE *fp;
    int i, c = 0;
    char battery_level_string[MAX_NUM_CHAR];
    char log_string[500];
    int battery_level_int;
    static int failure;

    if ((fp = fopen(battery_path_capacity, "r")) == NULL) {
        write_to_log("Error in get_battery_level: Battery capacity file could not be opened", true);
        exit(EXIT_FAILURE);
    }

    for (i = 0; i <= MAX_NUM_CHAR; i++) {

        if (!isdigit(c = getc(fp)))
            break;
        else
            battery_level_string[i] = (char)c;
    }

    fclose(fp);
    battery_level_string[i] = '\0';
    battery_level_int = atoi(battery_level_string);

    /* Battery capacity can apparently be reported as over 100, so this test will not check for too high values. */
    if (c != (int)'\n' || battery_level_int < 0) {
        snprintf(log_string, 500, "Battery capacity file could not be read properly. Last read char has ascii value: %d while it should be newline. Battery level read as: %d.",
                c, battery_level_int);

        /* Ensuring that log_string is properly terminated in case something goes horribly wrong. */
        for (i = 0; i < 500 && log_string[i]; i++)
            ;
        log_string[i] = '\0';

        write_to_log(log_string, true);
        failure++;
        if (failure == 3)
            exit(EXIT_FAILURE);
    }
    else
        failure = 0;

    return battery_level_int;
}

/********************************************************************
 *                                                                  *
 * A simple function to print an error message to stdout and syslog *
 * if provided an unknown argument.                                 *
 *                                                                  *
 ********************************************************************/
static void report_unknown_argument(const char *argument)
{
    int argument_length = (int)strlen(argument);
    char full_error[(strlen("Unknown argument: ") + argument_length + 1)];

    strcat(strncpy(full_error, "Unknown argument: ", strlen("Unknown argument: ") + 1), argument);

    printf("%s\n", full_error);
    write_to_log(full_error, true);
    exit(EXIT_FAILURE);
}

/*******************************************************************
 *                                                                 *
 * Writes provided string to the system log as error or info.      *
 *                                                                 *
 *******************************************************************/

static void write_to_log(const char *argument, bool to_error)
{
    openlog("sbatterymonitor", LOG_PID|LOG_CONS, LOG_USER);
    syslog(to_error ? LOG_ERR : LOG_INFO, "%s", argument);
    closelog();
}

/*******************************************************************
 *                                                                 *
 * A simple function which checks if a program is running.         *
 *                                                                 *
 * If program is running, it will store the pid in the pid array.  *
 * If there are multiple instances of program_name running, then   *
 * all PID values will be stored in the pid pointer.               *
 *                                                                 *
 * array_length shall specify how many elements may be stored in   *
 * the pid array                                                   *
 *                                                                 *
 *******************************************************************/

static bool is_program_running(const char *program_name, pid_t *pid, int array_length)
{
    FILE *pp;
    int c, i = 0;
    int number_of_pids = 0;
    int program_name_length = (int)strlen(program_name);
    char pid_string[30];
    char execute_string[(strlen("pidof ") + program_name_length + 1)];

    strcat(strncpy(execute_string, "pidof ", strlen("pidof ") + 1), program_name);
    
    if ((pp = popen(execute_string, "r")) == NULL) {
        write_to_log("Failure in is_program_running function. Could not open process stream with popen.", true);
        exit(EXIT_FAILURE);
    }

    while ((c = getc(pp)) != EOF) {
        if (c == (int)' ' || c == (int)'\n') {
            if (i > 0) {
                pid_string[i] = '\0';
                if (number_of_pids == array_length)
                    break;
                *(pid + number_of_pids++) = (pid_t)atoi(pid_string);
                i = 0;
            }
        } else
            if (isdigit(c))
                pid_string[i++] = (char)c;
        if (i >= 30) {
            write_to_log("PID string exceeds 29 digits, program not written to handle that.", true);
            exit(EXIT_FAILURE);
        }
    }
    pclose(pp);

    return number_of_pids;

}


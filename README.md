NAME
====
sbatterymonitor - a simple batterymonitor for laptops

SYNOPSIS
========
Usage: sbatterymonitor [option]

DEPENDENCIES
============
glibc

procps-ng

systemd - the default shutdown command require systemd, this can be changed in the source code

DESCRIPTION
===========
sbatterymonitor (simple batterymonitor) is a very simple monitor for laptop batteries. It simply reads the battery status. If the battery is not charging, the current battery capacity is read. At low levels the computer will shut down. The -x option will also make any X server shut down on low levels before shutting down the computer.

Specific behaviour can be controlled by editing the source code. I have plans to put the most likely candidates for editing in a seperate header file.

A systemd service file is provided which can be used to enable this to run as a sevice on boot. The service file assumes that the compiled sbatterymonitor is present in /usr/bin/

There is currently nothing to prevent you from running multiple instances of this in continuous mode, which would naturally enough be pointless.

The next plan is to remove the dependency on procps-ng, and instead traverse /proc to find Xorg pids. I simply want to reduce dependencies outside the bare necessities.

Suggestions and requests are of course welcome!

OPERATIONS
==========

-c  continuous mode: the program will check battery levels once every minute.

-x  terminate X server: the program will exit any running X server if battery levels drop below 16%

AUTHOR
======
Daniel Jenssen <daerandin@gmail.com>


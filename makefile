# sbatterymonitor - a simple laptop battery monitor which will shut down the system on low power

OUT = sbatterymonitor
CC = gcc
OBJ = sbatterymonitor.o
CFLAGS = -march=native -pthread -pedantic -Wall -Werror -Wextra -fstack-protector-strong -O2

sbatterymonitor: $(OBJ)
	$(CC) $(CFLAGS) -o $(OUT) $(OBJ)

sbatterymonitor.o: sbatterymonitor.c
	$(CC) $(CFLAGS) -c -o $@ $<

clean:
	$(RM) $(OUT) $(OBJ)

.PHONY: clean
